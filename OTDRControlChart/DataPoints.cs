﻿using System;
using System.IO;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace OTDRControlChart
{
    public class DataPoints
    {
        public static double[] GetDataPoints(string filePath = "C:\\Users\\fayla\\projects\\WindowsFormsAppDotNet1\\OTDRControlChart\\0083-32pon-smpon32nn_001_S13.SOR", string uri = "https://dev.aeros.dev/otdrapi/sorparser/parseone")
        {
            string sorFileJson = GetJasonData(filePath, uri);

            using JsonDocument doc = JsonDocument.Parse(sorFileJson);

            JsonElement dp = doc.RootElement.GetProperty("file")
                .GetProperty("dataPtsBlock")
                .GetProperty("scaleFactorInfos")[0].GetProperty("dataPoints");
            
            double[] datapoints = new double[dp.GetArrayLength()];
            
            int i = 0;
            int index = 0;
            JsonElement.ArrayEnumerator arr = dp.EnumerateArray();
            double max = 0;
            while (arr.MoveNext())
            {
                 double.TryParse(arr.Current.ToString(), out 
                    datapoints[i++]);

                if (datapoints[index] > max)
                {
                    max = datapoints[index];
                }

                index++;
            }
            
            for(i = 0; i < datapoints.Length; i++)
            {
                datapoints[i] = max - datapoints[i];
            }
            return datapoints;
        }
        public static string GetJasonData(string filePath= "C:\\Users\\fayla\\projects\\WindowsFormsAppDotNet1\\OTDRControlChart\\0083-32pon-smpon32nn_001_S13.SOR", string uri= "https://dev.aeros.dev/otdrapi/sorparser/parseone")
        {
            Uri u = new Uri(uri);

            using MultipartFormDataContent formContent = new MultipartFormDataContent("NKdKd9Yk");
            
            formContent.Headers.ContentType.MediaType = "multipart/form-data";
            
            Stream fileStream = System.IO.File.OpenRead(filePath);
            formContent.Add(new StreamContent(fileStream), "file", "file");

            Task<string> t = Task.Run(() => PostURI(u, formContent));
            t.Wait();
            
            return t.Result;

        }

        private static async Task<string> PostURI(Uri u, HttpContent c)
        {
            string response = string.Empty;
            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage result = await client.PostAsync(u, c);
                if (result.IsSuccessStatusCode)
                {
                    string resStr = await result.Content.ReadAsStringAsync();
                    
                    return resStr;

                }
            }
            return response;
        }
    }
}
