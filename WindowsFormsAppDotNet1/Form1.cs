﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Forms;
using OTDRControlChart;
using ScottPlot;

namespace WindowsFormsAppDotNet1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                InitialDirectory = @"C:\",
                Title = "Browse SOR Files",

                CheckFileExists = true,
                CheckPathExists = true,

                DefaultExt = "sor",
                Filter = "sor files (*.sor)|*.sor",
                FilterIndex = 2,
                RestoreDirectory = true,

                ReadOnlyChecked = true,
                ShowReadOnly = true
            };

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                selectedSORName.Text = openFileDialog1.FileName;
                plt_Load(sender, e);
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void plt_Load(object sender, EventArgs e)
        {
            
            if (string.IsNullOrEmpty(selectedSORName.Text))
            {
                return;
            }
            double[] dp = dp = DataPoints.GetDataPoints(selectedSORName.Text);

            Plot plot = this.plt.plt;

            // sample data
            double[] xs = DataGen.Consecutive(dp.Length);

            
            plot.AddScatter(xs, dp);
            
            plot.Title("SOR DataPoints Viewer");
            plot.XLabel("DataPoints Index");
            plot.YLabel("DataPoints DSF Value");
            plot.AxisAuto();
            plot.Legend();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
