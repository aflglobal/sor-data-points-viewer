﻿
namespace WindowsFormsAppDotNet1
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.selectedSORName = new System.Windows.Forms.TextBox();
            this.plt = new ScottPlot.FormsPlot();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.Info;
            this.button1.Location = new System.Drawing.Point(1073, 10);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(196, 34);
            this.button1.TabIndex = 0;
            this.button1.Text = "Select SOR";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // selectedSORName
            // 
            this.selectedSORName.Location = new System.Drawing.Point(14, 12);
            this.selectedSORName.Name = "selectedSORName";
            this.selectedSORName.Size = new System.Drawing.Size(1064, 31);
            this.selectedSORName.TabIndex = 2;
            this.selectedSORName.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // plt
            // 
            this.plt.BackColor = System.Drawing.Color.Transparent;
            this.plt.Location = new System.Drawing.Point(14, 52);
            this.plt.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.plt.Name = "plt";
            this.plt.Size = new System.Drawing.Size(1255, 572);
            this.plt.TabIndex = 4;
            this.plt.Load += new System.EventHandler(this.plt_Load);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1281, 626);
            this.Controls.Add(this.plt);
            this.Controls.Add(this.selectedSORName);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "SOR DataPoints Viewer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox selectedSORName;
        private ScottPlot.FormsPlot plt;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}

